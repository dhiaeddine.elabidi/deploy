# Axolotl (in progress)
*Yootel intelligent customer manager*
## About the software
| **Software card**         | **Details** |
|------------------|---|
| **Name**             | *Axolotl*  |
| **Acronym**            | *xltl* |
| **Latest release**   | *0.0.0*  |
| **Release date**  | *30/03/2020*  |
| **On working version**   | *0.0.1*  |
| **Author**           | *Yooth-It* |

## Environment setup
### Prerequisites
Install postgres sql:

```bash
$ sudo apt-get update
$ sudo apt-get install postgresql
$ sudo apt-get install python-psycopg2
$ sudo apt-get install libpq-dev
```

### Installation

### Running (& testing)

## Deployment 

## Contribute
To contribute to the project, please follow the following steps (GIT should be already installed):

1. Clone the project in a local repository with HTTPS:
```bash
$ git clone https://gitlab.com/YoothIt/axolotl.git
```
2. Change into the new project’s directory:
```bash
$ cd axolotl
```
3. Finally, you need to set up a new remote that points to the original project so that you can grab any changes and bring them into your local copy.
```bash
$ git remote add upstream https://gitlab.com/YoothIt/axolotl.git
```
 

## License 
Project License

## Documentation 
You can find documentation [here]() 


## extra 


