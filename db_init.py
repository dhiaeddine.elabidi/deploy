from app.utilities.commons import get_unique_id, generate_password
from app.models.database.tables.user import User
from app.models.database.tables.user_detail import UserDetail
from app.models.database.tables.category import Category
from app.models.database.tables.currency import Currency
from app.models.database.tables.service import Service
from app.models.database.tables.user_service import UserService
from app.models.database.tables.rsva_tariff_client import RsvaTariffClient
from app.models.database.tables.rsva_tariff_end_user import RsvaTariffEndUser
from app.models.database.tables.rsva_tariff import RsvaTariff
from app.models.number_model import NumberModel
from app import db

# static data
rates = {
    "G000": {
        "rate_call": 0.0,
        "rate_minute": 0.0
    },
    "B000": {
        "rate_call": 0.0,
        "rate_minute": 0.0
    },
    "A005": {
        "rate_call": 0.05,
        "rate_minute": 0.0
    },
    "A008": {
        "rate_call": 0.08,
        "rate_minute": 0.0
    },
    "A009": {
        "rate_call": 0.09,
        "rate_minute": 0.0
    },
    "A010": {
        "rate_call": 0.1,
        "rate_minute": 0.0
    },
    "A012": {
        "rate_call": 0.12,
        "rate_minute": 0.0
    },
    "A015": {
        "rate_call": 0.15,
        "rate_minute": 0.0
    },
    "A020": {
        "rate_call": 0.2,
        "rate_minute": 0.0
    },
    "A030": {
        "rate_call": 0.3,
        "rate_minute": 0.0
    },
    "A040": {
        "rate_call": 0.4,
        "rate_minute": 0.0
    },
    "A050": {
        "rate_call": 0.5,
        "rate_minute": 0.0
    },
    "A060": {
        "rate_call": 0.6,
        "rate_minute": 0.0
    },
    "A065": {
        "rate_call": 0.65,
        "rate_minute": 0.0
    },
    "A079": {
        "rate_call": 0.79,
        "rate_minute": 0.0
    },
    "A080": {
        "rate_call": 0.8,
        "rate_minute": 0.0
    },
    "A090": {
        "rate_call": 0.9,
        "rate_minute": 0.0
    },
    "A099": {
        "rate_call": 0.99,
        "rate_minute": 0.0
    },
    "A100": {
        "rate_call": 1.0,
        "rate_minute": 0.0
    },
    "A150": {
        "rate_call": 1.5,
        "rate_minute": 0.0
    },
    "A199": {
        "rate_call": 1.99,
        "rate_minute": 0.0
    },
    "A200": {
        "rate_call": 2.0,
        "rate_minute": 0.0
    },
    "A250": {
        "rate_call": 2.5,
        "rate_minute": 0.0
    },
    "A299": {
        "rate_call": 2.99,
        "rate_minute": 0.0
    },
    "A300": {
        "rate_call": 3.0,
        "rate_minute": 0.0
    },
    "D005": {
        "rate_call": 0.0,
        "rate_minute": 0.05
    },
    "D006": {
        "rate_call": 0.0,
        "rate_minute": 0.06
    },
    "D009": {
        "rate_call": 0.0,
        "rate_minute": 0.09
    },
    "D010": {
        "rate_call": 0.0,
        "rate_minute": 0.1
    },
    "D012": {
        "rate_call": 0.0,
        "rate_minute": 0.12
    },
    "D015": {
        "rate_call": 0.0,
        "rate_minute": 0.15
    },
    "D018": {
        "rate_call": 0.0,
        "rate_minute": 0.18
    },
    "D020": {
        "rate_call": 0.0,
        "rate_minute": 0.2
    },
    "D025": {
        "rate_call": 0.0,
        "rate_minute": 0.25
    },
    "D030": {
        "rate_call": 0.0,
        "rate_minute": 0.3
    },
    "D035": {
        "rate_call": 0.0,
        "rate_minute": 0.35
    },
    "D040": {
        "rate_call": 0.0,
        "rate_minute": 0.4
    },
    "D045": {
        "rate_call": 0.0,
        "rate_minute": 0.45
    },
    "D050": {
        "rate_call": 0.0,
        "rate_minute": 0.5
    },
    "D060": {
        "rate_call": 0.0,
        "rate_minute": 0.6
    },
    "D070": {
        "rate_call": 0.0,
        "rate_minute": 0.7
    },
    "D080": {
        "rate_call": 0.0,
        "rate_minute": 0.8
    },
    "M001": {
        "rate_call": 0.08,
        "rate_minute": 0.02
    },
    "M002": {
        "rate_call": 0.12,
        "rate_minute": 0.04
    },
    "M003": {
        "rate_call": 1.12,
        "rate_minute": 0.11
    },
    "M004": {
        "rate_call": 1.35,
        "rate_minute": 0.34
    },
    "M005": {
        "rate_call": 1.49,
        "rate_minute": 1.49
    },
    "M006": {
        "rate_call": 1.57,
        "rate_minute": 0.45
    },
    "M007": {
        "rate_call": 1.89,
        "rate_minute": 0.5
    },
    "M008": {
        "rate_call": 1.99,
        "rate_minute": 0.5
    },
    "M009": {
        "rate_call": 1.99,
        "rate_minute": 0.99
    },
    "M010": {
        "rate_call": 1.99,
        "rate_minute": 1.99
    },
    "M011": {
        "rate_call": 2.19,
        "rate_minute": 0.5
    },
    "M012": {
        "rate_call": 2.19,
        "rate_minute": 2.19
    },
    "M013": {
        "rate_call": 2.5,
        "rate_minute": 0.5
    },
    "M014": {
        "rate_call": 2.5,
        "rate_minute": 0.99
    },
    "M015": {
        "rate_call": 2.5,
        "rate_minute": 2.5
    },
    "M016": {
        "rate_call": 2.75,
        "rate_minute": 2.75
    },
    "M017": {
        "rate_call": 2.99,
        "rate_minute": 0.99
    },
    "M018": {
        "rate_call": 2.99,
        "rate_minute": 1.99
    },
    "M019": {
        "rate_call": 2.99,
        "rate_minute": 2.99
    },
    "M020": {
        "rate_call": 3.25,
        "rate_minute": 3.25
    },
    "M021": {
        "rate_call": 3.5,
        "rate_minute": 3.5
    },
    "M022": {
        "rate_call": 2.9,
        "rate_minute": 0.5
    }
}

# create categories
admin_category = Category(
    id=1,
    code="admin",
    name="Admin",
    description="Super user of the system"
)
user_category = Category(
    id=2,
    code="user",
    name="User",
    description="Normal user of the system"
)
# create currencies
euro = Currency(
    id=1,
    name="Euro",
    iso_code="EUR",
    icon="euro-sign",
    exchange_rate=1
)
dollar = Currency(
    id=2,
    name="Dollar",
    iso_code="USD",
    icon="dollar-sign",
    exchange_rate=1.08
)
# create services
rsva = Service(
    id=1,
    service_code="rsva",
    name="RSVA",
    level="info",
    icon="broadcast-tower",
    description="RSVA TELECOM"
)
tsip = Service(
    id=2,
    service_code="tsip",
    name="TRUNK SIP",
    level="warning",
    icon="cloud-download-alt",
    description="Trunk Sip telecomminication service"
)
cntx = Service(
    id=3,
    service_code="cntx",
    name="CENTREX",
    level="danger",
    icon="phone-square-alt",
    description="Centrex telecomminication service"
)
# create mock users
admin_ref = get_unique_id()
user_ref = get_unique_id()
admin = User(
    id=admin_ref,
    username="axolotl_admin",
    password=generate_password(),
    valid=True,
    category_id=1,
    currency_id=1
)
user = User(
    id=user_ref,
    username="axolotl_user",
    password=generate_password(),
    valid=True,
    category_id=2,
    currency_id=1
)
user_detail = UserDetail(
    first_name="Axolotl",
    last_name="User",
    organism="Yootel",
    email="user@yootel.io",
    user_id=user_ref
)
admin_detail = UserDetail(
    first_name="Axolotl",
    last_name="Admin",
    organism="Yootel",
    email="admin@yootel.io",
    user_id=admin_ref
)
# attribute service to user
user_service = UserService(
    user_id=user_ref,
    service_id=1
)
# add to database
db.session.add(admin_category)
db.session.add(user_category)
db.session.add(euro)
db.session.add(dollar)
db.session.add(rsva)
db.session.add(tsip)
db.session.add(cntx)
db.session.add(admin)
db.session.add(user)
db.session.add(user_detail)
db.session.add(admin_detail)
db.session.add(user_service)

# add rates
for rate in rates:
    rate = RsvaTariffEndUser(
        code=rate,
        call_rate=rates[rate]['rate_call'],
        minute_rate=rates[rate]['rate_minute']
    )
    db.session.add(rate)
number_1 = {
    'country_code': "33",
    'phone_code': "0898400005",
    'end_user_tariff': 'A015',
    'client_tariff': {
        'call_rate': 2,
        'interval_1': 40,
        'interval_n': 15,
        'price_interval_1': 1,
        'price_interval_n': 0.08
    }
}
number_2 = {
    'country_code': "33",
    'phone_code': "0898400014",
    'end_user_tariff': 'A015',
    'client_tariff': {
        'call_rate': 2,
        'interval_1': 40,
        'interval_n': 15,
        'price_interval_1': 1,
        'price_interval_n': 0.08
    }
}
number_3 = {
    'country_code': "33",
    'phone_code': "0890140015",
    'end_user_tariff': 'A015',
    'client_tariff': {
        'call_rate': 2,
        'interval_1': 40,
        'interval_n': 15,
        'price_interval_1': 1,
        'price_interval_n': 0.08
    }
}
number_4 = {
    'country_code': "33",
    'phone_code': "0898400288",
    'end_user_tariff': 'A015',
    'client_tariff': {
        'call_rate': 2,
        'interval_1': 40,
        'interval_n': 15,
        'price_interval_1': 1,
        'price_interval_n': 0.08
    }
}
NumberModel.add(number_1)
NumberModel.add(number_2)
NumberModel.add(number_3)
NumberModel.add(number_4)
# commit changes
db.session.commit()
