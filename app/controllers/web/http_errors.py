from app.utilities.logger import *
from app.utilities.http_errors import HttpError
from flask import Blueprint, request, render_template, redirect
import traceback

# create a new blueprint for the controller
web_http_error = Blueprint('web_http_error', __name__)


# define the routes
@web_http_error.route('/<int:error_code>', methods=['GET'])
def error(error_code):
    """
    Http error controller
    :param error_code: http error code
    :return:
    """
    try:
        log_info(
            message="Error Page",
            ip_address=request.remote_addr
        )
        if error_code in HttpError:
            message = HttpError[error_code]
        else:
            return redirect('/error/404')
        return render_template("/http_errors.html",
                               message=message)
    except Exception as e:
        log_error(
            message=str(e),
            description=traceback.format_exc(),
            ip_address=request.remote_addr
        )
        return redirect('/error/500')
