"""
File: example.py
Example web application routes definition
"""
from app.utilities.layout_setup import layout
from app.utilities.menu import get_menu
from app.utilities.logger import *
from app.models.user_model import UserModel
from app.models.service_model import ServiceModel
from flask import Blueprint, render_template, request, redirect, session
from flask_login import login_required, current_user
import traceback


# create a new blueprint for the controller
web_example = Blueprint('web_example', __name__)


# define the routes
@web_example.route('/', methods=['GET'])
@login_required
def example_page():
    """
    Example web application route call
    :return: default web page
    """
    try:
        page = {
            'layout': layout,
            'user': UserModel.get_user(current_user.id),
            'title': "Example",
            'menu': get_menu(session['service']),
            'services': ServiceModel.get_user_services(current_user.id)
        }
        return render_template('/services/example.html',
                               page=page)
    except Exception as e:
        log_error(
            message=str(e),
            description=traceback.format_exc(),
            ip_address=request.remote_addr
        )
        return redirect('/error/500')
