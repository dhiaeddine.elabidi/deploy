"""
File: dashboard.py
Dashboard web application routes definition
"""
from app.utilities.layout_setup import layout
from app.utilities.menu import get_admin_menu
from app.utilities.logger import *
from app.models.user_model import UserModel
from app.models.service_model import ServiceModel
from flask import Blueprint, render_template, request, redirect
from flask_login import login_required, current_user
import traceback


# create a new blueprint for the controller
web_admin_dashboard = Blueprint('web_admin_dashboard', __name__)


# define the routes
@web_admin_dashboard.route('/', methods=['GET'])
@login_required
def dashboard_admin_page():
    """
    Admin dashboard web application route call
    :return: admin dashboard web page
    """
    try:
        user = UserModel.get_user(current_user.id)
        if user['category']['code'] == 'admin':
            page = {
                'layout': layout,
                'user': user,
                'title': "Admin dashboard",
                'menu': get_admin_menu(),
                'services': ServiceModel.get_user_services(current_user.id)
            }
            return render_template('/admin/dashboard.html',
                                   page=page)
        else:
            return redirect('/error/403')
    except Exception as e:
        log_error(
            message=str(e),
            description=traceback.format_exc(),
            ip_address=request.remote_addr
        )
        return redirect('/error/500')
