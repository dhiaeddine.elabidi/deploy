"""
File: user.py
Admin users management routes definition
"""
from app.utilities.layout_setup import layout
from app.utilities.menu import get_admin_menu
from app.utilities.logger import *
from app.models.user_model import UserModel
from app.models.service_model import ServiceModel
from app.models.forms.request_membership_form import RequestMembershipForm
from app.models.forms.edit_user_form import EditUserForm
from flask import Blueprint, render_template, request, redirect
from flask_login import login_required, current_user
import traceback


# create a new blueprint for the controller
web_admin_users = Blueprint('web_admin_users', __name__)


# define the routes
@web_admin_users.route('/requests', methods=['GET', 'POST'])
@login_required
def requests_admin_page():
    """
    Add users web application route call
    :return: add users admin web page
    """
    try:
        user = UserModel.get_user(current_user.id)
        if user['category']['code'] == 'admin':
            message = {
                'success': None,
                'error': None
            }
            page = {
                'layout': layout,
                'user': user,
                'title': "Users",
                'menu': get_admin_menu(),
                'services': ServiceModel.get_user_services(current_user.id)
            }
            request_membership_form = RequestMembershipForm()
            if request.method == 'POST' and request_membership_form.validate_on_submit():
                UserModel.add_user(
                    username=request_membership_form.username.data,
                    first_name=request_membership_form.first_name.data,
                    last_name=request_membership_form.last_name.data,
                    email=request_membership_form.email.data,
                    country_code=request_membership_form.country_code.data,
                    number=request_membership_form.number.data,
                    organism=request_membership_form.organism.data
                )
                message['success'] = "0001"
            requests = UserModel.get_membership_requests()
            return render_template('/admin/add_users.html',
                                   requests=requests,
                                   request_membership_form=request_membership_form,
                                   message=message,
                                   page=page)
        else:
            return redirect('/error/403')
    except Exception as e:
        log_error(
            message=str(e),
            description=traceback.format_exc(),
            ip_address=request.remote_addr
        )
        return redirect('/error/500')


@web_admin_users.route('/requests/delete/<string:reference>', methods=['GET'])
@login_required
def delete_request(reference):
    try:
        user = UserModel.get_user(current_user.id)
        if user['category']['code'] == 'admin':
            UserModel.delete_user(reference)
            return redirect('/admin/users/requests')
        else:
            return redirect('/error/403')
    except Exception as e:
        log_error(
            message=str(e),
            description=traceback.format_exc(),
            ip_address=request.remote_addr
        )
        return redirect('/error/500')


@web_admin_users.route('/requests/accept/<string:reference>', methods=['GET'])
@login_required
def accept_request(reference):
    try:
        user = UserModel.get_user(current_user.id)
        if user['category']['code'] == 'admin':
            UserModel.accept_user(reference)
            return redirect('/admin/users/requests')
        else:
            return redirect('/error/403')
    except Exception as e:
        log_error(
            message=str(e),
            description=traceback.format_exc(),
            ip_address=request.remote_addr
        )
        return redirect('/error/500')


@web_admin_users.route('/list', methods=['GET'])
@login_required
def users_list_admin_page():
    try:
        user = UserModel.get_user(current_user.id)
        if user['category']['code'] == 'admin':
            message = {
                'success': None,
                'error': None
            }
            page = {
                'layout': layout,
                'user': user,
                'title': "Users",
                'menu': get_admin_menu(),
                'services': ServiceModel.get_user_services(current_user.id)
            }
            users = UserModel.get_users()
            return render_template('/admin/users_list.html',
                                   users=users,
                                   message=message,
                                   page=page)
        else:
            return redirect('/error/403')
    except Exception as e:
        log_error(
            message=str(e),
            description=traceback.format_exc(),
            ip_address=request.remote_addr
        )
        return redirect('/error/500')


@web_admin_users.route('/edit/<string:user_id>', methods=['GET', 'POST'])
@login_required
def edit_user(user_id):
    try:
        user = UserModel.get_user(current_user.id)
        if user['category']['code'] == 'admin':
            message = {
                'success': None,
                'error': None
            }
            page = {
                'layout': layout,
                'user': user,
                'title': "Users",
                'menu': get_admin_menu(),
                'services': ServiceModel.get_user_services(current_user.id)
            }
            user_to_edit = UserModel.get_user(user_id)
            edit_user_form = EditUserForm()
            return render_template('/admin/edit_user.html',
                                   user=user_to_edit,
                                   edit_user_form=edit_user_form,
                                   message=message,
                                   page=page)
        else:
            return redirect('/error/403')
    except IndexError:
        return redirect('/error/404')
    except Exception as e:
        log_error(
            message=str(e),
            description=traceback.format_exc(),
            ip_address=request.remote_addr
        )
        return redirect('/error/500')