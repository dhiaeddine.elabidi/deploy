"""
File: index.py
Default web application routes definition
"""
from app.utilities.logger import *
from app.models.category_model import CategoryModel
from flask import Blueprint, redirect, request, session
from flask_login import current_user
import traceback


# create a new blueprint for the controller
web_index = Blueprint('web_index', __name__)


# define the routes
@web_index.route('/', methods=['GET'])
@web_index.route('/index', methods=['GET'])
def index():
    """
    Basic web application route call
    :return: default web page
    """
    try:
        if current_user.is_authenticated:
            session['lang'] = current_user.lang
            session['service'] = session['service'] if 'service' in session else "home"
            next = request.args.get('next') if request.args.get('next') else None
            if next is None:
                if CategoryModel.get_category(current_user.category_id)['code'] == 'admin':
                    return redirect('/admin/dashboard')
                else:
                    return redirect('/dashboard')
            else:
                return redirect(next)
        else:
            return redirect('/login')
    except Exception as e:
        log_error(
            message=str(e),
            description=traceback.format_exc(),
            ip_address=request.remote_addr
        )
        return str(e)+":"+traceback.format_exc()
