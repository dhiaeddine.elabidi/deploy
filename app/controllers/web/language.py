"""
File: language.py
Translate, change language and routes definition
"""
from app.utilities.logger import *
from app.models.user_model import UserModel
from flask import Blueprint, request, redirect
from flask_login import current_user, login_required
import traceback

# create a new blueprint for the controller
web_language = Blueprint('web_language', __name__)


# define the routes
@web_language.route('/set/<string:lang>')
@login_required
def set_language(lang):
    try:
        UserModel.set_language(current_user.id, lang)
        return redirect('/index')
    except Exception as e:
        log_error(
            message=str(e),
            description=traceback.format_exc(),
            ip_address=request.remote_addr
        )
        return redirect('/error/500')
