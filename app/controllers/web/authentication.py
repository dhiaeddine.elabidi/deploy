"""
File: authentication.py
Authentication logic and routes definition
"""
from app.utilities.logger import *
from app.utilities.layout_setup import layout
from app.models.forms.login_form import LoginForm
from app.models.forms.reset_password_form import ResetPasswordForm
from app.models.forms.request_membership_form import RequestMembershipForm
from app.models.database.tables.user import User
from app.models.user_model import UserModel
from app.utilities.commons import hashing, verify_fields
from flask import Blueprint, render_template, request, redirect
from flask_login import login_user, logout_user, current_user, login_required
from datetime import datetime
import traceback


# create a new blueprint for the controller
web_authentication = Blueprint('web_authentication', __name__)


# define the routes
@web_authentication.route('/login', methods=['GET', 'POST'])
@web_authentication.route('/signin', methods=['GET', 'POST'])
def login_page():
    """
    Login page controller
    :return: login web page
    """
    try:
        if current_user.is_authenticated:
            return redirect('/index')
        else:
            log_info(message="Login form web page",
                     ip_address=request.remote_addr)
            page = {
                'layout': layout,
                'title': "Login"
            }
            message = None
            next = request.args.get('next') if request.args.get('next') else None
            login_form = LoginForm()
            if request.method == 'POST':
                if login_form.validate_on_submit():
                    user = User.query.filter_by(username=login_form.username.data).first()
                    if user:
                        if user.valid:
                            if user.first_login:
                                if user.password == login_form.password.data:
                                    login_user(user)
                                    user.last_login = datetime.utcnow()
                                    db.session.commit()
                                    return redirect('/reset_password')
                                else:
                                    message = "Cannot login, username and password does not match."
                            else:
                                if user.password == hashing(login_form.password.data):
                                    login_user(user, remember=login_form.remember_me.data)
                                    user.last_login = datetime.utcnow()
                                    db.session.commit()
                                    if next is None:
                                        return redirect('/index')
                                    else:
                                        return redirect('/index?next=' + next)
                                else:
                                    message = "Cannot login, username and password does not match."
                        else:
                            message = login_form.username.data + " is not valid, please contact system" \
                                                                          " administrator for further information!"
                    else:
                        message = "Cannot login, username and password does not match."
                else:
                    if not verify_fields(login_form.username.data, login_form.password.data):
                        message = "Form fields are not valid, please fill with valid data!"
            return render_template('/authentication/login.html',
                                   login_form=login_form,
                                   message=message,
                                   page=page)
    except Exception as e:
        log_error(
            message=str(e),
            description=traceback.format_exc(),
            ip_address=request.remote_addr
        )
        return redirect('/error/500')


@web_authentication.route('/reset_password', methods=['GET', 'POST'])
@login_required
def reset_password():
    """
    Reset password controller
    :return: reset password page
    """
    try:
        user_id = current_user.id
        message = {
            'error': None,
            'success': None
        }
        page = {
            'layout': layout,
            'title': "Reset Password"
        }
        reset_password_form = ResetPasswordForm()
        if request.method == 'POST':
            if reset_password_form.validate_on_submit():
                user = User.query.filter_by(id=user_id).first()
                if user.password == reset_password_form.old_password.data:
                    user.password = hashing(reset_password_form.new_password.data)
                    user.first_login = False
                    user.last_modified = datetime.utcnow()
                    db.session.commit()
                    logout_user()
                    message['success'] = "Password reset."
                else:
                    message['error'] = "Wrong password, please verify and try again."
            else:
                if not verify_fields(reset_password_form.new_password.data,
                                     reset_password_form.old_password.data,
                                     reset_password_form.confirm_password.data):
                    message['error'] = "Form fields length are not [8-64] length."
                elif reset_password_form.new_password != reset_password_form.confirm_password:
                    message['error'] = "New passwords doest not match."
                else:
                    message['error'] = "Something went wrong, please try again."
        return render_template('/authentication/reset_password.html',
                               reset_password_form=reset_password_form,
                               page=page,
                               message=message)
    except Exception as e:
        log_error(
            message=str(e),
            description=traceback.format_exc(),
            ip_address=request.remote_addr
        )
        return redirect('/error/500')


@web_authentication.route('/request_membership', methods=['GET', 'POST'])
def request_membership_page():
    """
    Request membership
    :return: request membership page
    """
    try:
        message = {
            'error': None,
            'success': None
        }
        page = {
            'layout': layout,
            'title': "Reset Password"
        }
        request_membership_form = RequestMembershipForm()
        if request.method == 'POST':
            if request_membership_form.validate_on_submit():
                if request_membership_form.agreement.data:
                    UserModel.add_user(
                        username=request_membership_form.username.data,
                        first_name=request_membership_form.first_name.data,
                        last_name=request_membership_form.last_name.data,
                        email=request_membership_form.email.data,
                        country_code=request_membership_form.country_code.data,
                        number=request_membership_form.number.data,
                        organism=request_membership_form.organism.data
                    )
                    message['success'] = "Your request has been sent, we will contact you as soon as possible!"
                else:
                    message['error'] = "Please accept membership agreement before submitting your request."
            else:
                if not verify_fields(request_membership_form.username.data,
                                     request_membership_form.first_name.data,
                                     request_membership_form.last_name.data,
                                     request_membership_form.number.data,
                                     request_membership_form.email.data,
                                     request_membership_form.organism.data):
                    message['error'] = "Form fields length are not [8-64] length."
                else:
                    message['error'] = "Something went wrong, please try again."
        return render_template('/authentication/request_membership.html',
                               request_membership_form=request_membership_form,
                               page=page,
                               message=message)
    except Exception as e:
        log_error(
            message=str(e),
            description=traceback.format_exc(),
            ip_address=request.remote_addr
        )
        return redirect('/error/500')


@web_authentication.route('/logout', methods=['GET'])
@web_authentication.route('/signout', methods=['GET'])
@login_required
def logout_page():
    """
        Logout controller
        :return: redirect to login page
    """
    try:
        log_info(
            message="User Logout",
            user_reference=current_user.id,
            ip_address=request.remote_addr
        )
        logout_user()
        return redirect('/index')
    except Exception as e:
        log_error(
            message=str(e),
            description=traceback.format_exc(),
            ip_address=request.remote_addr
        )
        return redirect('/error/500')
