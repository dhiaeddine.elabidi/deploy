"""
File: invoices.py
Invoiices controller
"""
from app.utilities.layout_setup import layout
from app.utilities.menu import get_menu
from app.utilities.logger import *
from app.models.user_model import UserModel
from app.models.service_model import ServiceModel
from app.models.rsva_invoice_model import RSVAInvoiceModel
from flask import Blueprint, render_template, request, redirect, session
from flask_login import login_required, current_user
import traceback


# create a new blueprint for the controller
web_invoices = Blueprint('web_invoices', __name__)


# define the routes
@web_invoices.route('/rsva/<int:month>/<int:year>', methods=['GET'])
@login_required
def rsva_invoice(month, year):
    """
    Invoice rsva service
    :return:
    """
    try:
        page = {
            'layout': layout,
            'user': UserModel.get_user(current_user.id),
            'title': "RSVA Invoice",
            'menu': get_menu(session['service']),
            'services': ServiceModel.get_user_services(current_user.id)
        }
        invoice = RSVAInvoiceModel.get(current_user.id, month, year)
        return render_template('/invoices/rsva.html',
                               invoice=invoice,
                               page=page)
    except Exception as e:
        log_error(
            message=str(e),
            description=traceback.format_exc(),
            ip_address=request.remote_addr
        )
        return redirect('/error/500')


@web_invoices.route('/rsva', methods=['GET'])
@login_required
def rsva_invoices_list():
    """
    Invoice rsva service
    :return:
    """
    try:
        page = {
            'layout': layout,
            'user': UserModel.get_user(current_user.id),
            'title': "RSVA Invoices",
            'menu': get_menu(session['service']),
            'services': ServiceModel.get_user_services(current_user.id)
        }
        invoices = RSVAInvoiceModel.get_all(current_user.id)
        return render_template('/invoices/rsva_list.html',
                               invoices=invoices,
                               page=page)
    except Exception as e:
        log_error(
            message=str(e),
            description=traceback.format_exc(),
            ip_address=request.remote_addr
        )
        return redirect('/error/500')
