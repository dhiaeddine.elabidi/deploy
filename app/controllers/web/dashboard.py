"""
File: dashboard.py
Dashboard web application routes definition
"""
from app.utilities.layout_setup import layout
from app.utilities.menu import get_menu
from app.utilities.logger import *
from app.models.user_model import UserModel
from app.models.service_model import ServiceModel
from flask import Blueprint, render_template, request, redirect, session
from flask_login import login_required, current_user
import traceback


# create a new blueprint for the controller
web_dashboard = Blueprint('web_dashboard', __name__)


# define the routes
@web_dashboard.route('/home', methods=['GET'])
@web_dashboard.route('/', methods=['GET'])
@login_required
def home_dashboard_page():
    """
    Dashboard home web application route call
    :return: home dashboard web page
    """
    try:
        page = {
            'layout': layout,
            'user': UserModel.get_user(current_user.id),
            'title': "Dashboard",
            'menu': get_menu(session['service']),
            'services': ServiceModel.get_user_services(current_user.id)
        }
        return render_template('/dashboards/home.html',
                               page=page)
    except Exception as e:
        log_error(
            message=str(e),
            description=traceback.format_exc(),
            ip_address=request.remote_addr
        )
        return redirect('/error/500')


@web_dashboard.route('/rsva', methods=['GET'])
@login_required
def rsva_dashboard_page():
    """
    Dashboard rsva web application route call
    :return: rsva dashboard web page
    """
    try:
        page = {
            'layout': layout,
            'user': UserModel.get_user(current_user.id),
            'title': "RSVA Dashboard",
            'menu': get_menu(session['service']),
            'services': ServiceModel.get_user_services(current_user.id)
        }
        return render_template('/dashboards/home.html',
                               page=page)
    except Exception as e:
        log_error(
            message=str(e),
            description=traceback.format_exc(),
            ip_address=request.remote_addr
        )
        return redirect('/error/500')

