"""
File: logger.py
Tracking application behaviour
Level of logging: Info, Error, Warning, Debug, Fatal
"""
from app.models.database.tables.log import Log
from app.utilities.configuration import configuration
from app import db
import logging

# config logger
logger = logging.getLogger('simple_example')
logger.setLevel(logging.DEBUG)
ch = logging.StreamHandler()
ch.setLevel(logging.INFO)
formatter = logging.Formatter('%(levelname)s:%(asctime)s: %(message)s')
ch.setFormatter(formatter)
logger.addHandler(ch)
# initialize configuration variable
config = configuration()


def log_info(message=None, description=None, ip_address=None, user_reference=None):
    """
    Log an info message; track application behavior
    :param message: info message to log
    :param description: details about the message
    :param ip_address: ip address, source to message
    :param user_reference: user code id
    :return: None
    """
    if 'INFO' in config['logger']['log_level']:
        if message is not None:
            logger.info(message + ' ' + str(description))
            log = Log(
                log_level='INFO',
                message=message[0:300] if message is not None else None,
                description=description[0:1000] if description is not None else None,
                ip_address=ip_address[0:16] if ip_address is not None else None,
                user_reference=user_reference[0:80] if user_reference is not None else None
            )
            db.session.add(log)
            db.session.commit()
        else:
            raise TypeError('Cannot log a empty message.')


def log_error(message=None, description=None, ip_address=None, user_reference=None):
    """
    Log an error message; track an error while app is running
    :param message: error message to log
    :param description: details about the message
    :param ip_address: ip address, source to message
    :param user_reference: user code id
    :return: None
    """
    if 'ERROR' in config['logger']['log_level']:
        if message is not None:
            logger.error(message + ' ' + str(description))
            log = Log(
                log_level='ERROR',
                message=message[0:300] if message is not None else None,
                description=description[0:1000] if description is not None else None,
                ip_address=ip_address[0:16] if ip_address is not None else None,
                user_reference=user_reference[0:80] if user_reference is not None else None
            )
            db.session.add(log)
            db.session.commit()
        else:
            raise TypeError('Cannot log a empty message.')


def log_debug(message=None, description=None, ip_address=None, user_reference=None):
    """
    Log a debug message to database and write it to console
    :param message: debug message to write
    :param description: details about the message
    :param ip_address: ip address, source to message
    :param user_reference: user code id
    :return: None
    """
    if 'DEBUG' in config['logger']['log_level']:
        logger.debug(message + ' ' + str(description))
        log = Log(
            log_level='DEBUG',
            message=message[0:300] if message is not None else None,
            description=description[0:1000] if description is not None else None,
            ip_address=ip_address[0:16] if ip_address is not None else None,
            user_reference=user_reference[0:80] if user_reference is not None else None
        )
        db.session.add(log)
        db.session.commit()


def log_warning(message=None, description=None, ip_address=None, user_reference=None):
    """
    Log a warning message; track a inappropriate usage that can be annoying
    :param message: error message to log
    :param description: details about the message
    :param ip_address: ip address, source to message
    :param user_reference: user code id
    :return: None
    """
    if 'WARNING' in config['logger']['log_level']:
        if message is not None:
            logger.warning(message + ' ' + str(description))
            log = Log(
                log_level='WARNING',
                message=message[0:300] if message is not None else None,
                description=description[0:1000] if description is not None else None,
                ip_address=ip_address[0:16] if ip_address is not None else None,
                user_reference=user_reference[0:80] if user_reference is not None else None
            )
            db.session.add(log)
            db.session.commit()
        else:
            raise TypeError('Cannot log a empty message.')


def log_fatal(message=None, description=None, ip_address=None, user_reference=None):
    """
    Log a critical message; track a critical behavior disabling the app run or leading to a crash
    :param message: fatal message to log
    :param description: details about the message
    :param ip_address: ip address, source to message
    :param user_reference: user code id
    :return: None
    """
    if 'FATAL' in config['logger']['log_level']:
        if message is not None:
            logger.fatal(message + ' ' + str(description))
            log = Log(
                log_level='FATAL',
                message=message[0:300] if message is not None else None,
                description=description[0:1000] if description is not None else None,
                ip_address=ip_address[0:16] if ip_address is not None else None,
                user_reference=user_reference[0:80] if user_reference is not None else None
            )
            db.session.add(log)
            db.session.commit()
        else:
            raise TypeError('Cannot log a empty message.')
