"""
File: menu.py
Implement application menu logic
"""

reg_user_menu = [
    {
        'name': "HOME",
        'code': "home",
        'subs': [
            {
                'name': "Dashboard",
                'icon': "tachometer-alt",
                'link': "/dashboard",
                'badge': None,
                'subs': []
            },
            {
                'name': "Payment",
                'icon': "money-bill-wave",
                'subs': [
                    {
                        'name': "Debit/Credit Cards",
                        'icon': "credit-card",
                        'link': "/payment/cards",
                        'badge': None,
                        'subs': []
                    },
                    {
                        'name': "Payment History",
                        'icon': "comment-dollar",
                        'link': "/payment/history",
                        'badge': None,
                        'subs': []
                    },
                ]
            },
            {
                'name': "Invoices",
                'icon': "file-invoice",
                'link': "/invoices",
                'badge': None,
                'subs': []
            },
        ]
    },
    {
        'name': "RSVA",
        'code': "rsva",
        'subs': [
            {
                'name': "Dashboard",
                'icon': "tachometer-alt",
                'link': "/dashboard/rsva",
                'badge': None,
                'subs': []
            },
            {
                'name': "Surtaxed Numbers",
                'icon': "phone-square-alt",
                'subs': [
                    {
                        'name': "Dynamic",
                        'icon': "compress-arrows-alt",
                        'link': "/rsva/dynamic",
                        'badge': None,
                        'subs': []
                    },
                    {
                        'name': "Static",
                        'icon': "expand-arrows-alt",
                        'link': "/rsva/dynamic",
                        'badge': None,
                        'subs': []
                    },
                ]
            },
            {
                'name': "Invoices",
                'icon': "file-invoice",
                'link': "/invoices/rsva",
                'badge': None,
                'subs': []
            },
        ]
    },
    {
        'name': "TRUNK SIP",
        'code': "tsip",
        'subs': [
            {
                'name': "Dashboard",
                'icon': "tachometer-alt",
                'link': "/dashboard/tsip",
                'badge': None,
                'subs': []
            },
            {
                'name': "Services",
                'icon': "cogs",
                'subs': [
                    {
                        'name': "Trusted Numbers",
                        'icon': "user-secret",
                        'link': "/tsip/service/trusted_number",
                        'badge': None,
                        'subs': []
                    },
                    {
                        'name': "Smart Dialing",
                        'icon': "fire",
                        'link': "/tsip/service/smart_dial",
                        'badge': None,
                        'subs': []
                    },
                ]
            },
            {
                'name': "Routing",
                'icon': "network-wired",
                'subs': [
                    {
                        'name': "Trunks",
                        'icon': "exchange-alt",
                        'link': "/tsip/routing/trunks",
                        'badge': None,
                        'subs': []
                    },
                    {
                        'name': "Incoming Routing",
                        'icon': "arrow-circle-down",
                        'link': "/tsip/routing/incoming",
                        'badge': None,
                        'subs': []
                    },
                ]
            },
            {
                'name': "Invoices",
                'icon': "file-invoice",
                'link': "/invoices/tsip",
                'badge': None,
                'subs': []
            },
        ]
    },
    {
        'name': "CENTREX",
        'code': "cntx",
        'subs': [
            {
                'name': "Dashboard",
                'icon': "tachometer-alt",
                'link': "/dashboard/cntx",
                'badge': None,
                'subs': []
            },
            {
                'name': "Invoices",
                'icon': "file-invoice",
                'link': "/invoices/cntx",
                'badge': None,
                'subs': []
            },
        ]
    }
]
admin_user_menu = [
    {
        'name': "ADMIN MENU",
        'subs': [
            {
                'name': "Dashboard",
                'icon': "tachometer-alt",
                'link': "/admin/dashboard",
                'badge': None,
                'subs': []
            },
            {
                'name': "Users",
                'icon': "users",
                'subs': [
                    {
                        'name': "Users list",
                        'icon': "address-book",
                        'link': "/admin/users/list",
                        'badge': None,
                        'subs': []
                    },
                    {
                        'name': "Membership requests",
                        'icon': "user-plus",
                        'link': "/admin/users/requests",
                        'badge': None,
                        'subs': []
                    },
                ]
            },
        ]
    }
]
support_menu = [
    {
        'name': "APPLICATION",
        'subs': [
            {
                'name': "Support",
                'icon': "headset",
                'link': "/support",
                'badge': {
                    'content': "2",
                    'level': "danger"
                },
                'subs': []
            },
            {
                'name': "Documentation",
                'icon': "file-alt",
                'link': "/documentation",
                'badge': None,
                'subs': []
            },
        ]
    }
]


def get_menu(service="home"):
    user_menu = None
    if service:
        i = 0
        for item in reg_user_menu:
            if item['code'] == service:
                break
            else:
                i += 1
        user_menu = reg_user_menu[i]
    else:
        user_menu = reg_user_menu[0]
    return [user_menu] + support_menu


def get_admin_menu():
    return admin_user_menu + support_menu
