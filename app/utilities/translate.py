"""
File: translate.py
Application to support multi language in flask application
"""
import os
import json

path = os.path.join("app", "utilities", "langs")


def translate(word, lang_code):
    if lang_code.lower() == 'fr':
        with open(os.path.join(path, 'fr.json')) as json_file:
            fr = json.load(json_file)
            if word in fr:
                return fr[word]
            else:
                return word
    elif lang_code.lower() == 'gb':
        with open(os.path.join(path, 'gb.json')) as json_file:
            en = json.load(json_file)
            if word in en:
                return en[word]
            else:
                return word
    elif lang_code.lower() == 'us':
        with open(os.path.join(path, 'us.json')) as json_file:
            en = json.load(json_file)
            if word in en:
                return en[word]
            else:
                return word
    elif lang_code.lower() == 'de':
        with open(os.path.join(path, 'de.json')) as json_file:
            en = json.load(json_file)
            if word in en:
                return en[word]
            else:
                return word
    else:
        return word


def format_date(date, lang_code):
    if lang_code.lower() == 'fr':
        # french date format
        return date.strftime("%d/%m/%Y")
    elif lang_code.lower() == 'us':
        # us date format
        return date.strftime("%b %d, %Y")
    elif lang_code.lower() == 'gb':
        # gb date format
        return date.strftime("%d %b %Y")
    elif lang_code.lower() == 'de':
        # de date format
        return date.strftime("%d.%m.%Y")
    else:
        # default case: uk date format
        return date.strftime("%d %b %Y")


def format_timestamp(date, lang_code):
    if lang_code.lower() == 'fr':
        # french date format
        return date.strftime("%d/%m/%Y - %H:%M")
    elif lang_code.lower() == 'us':
        # us date format
        return date.strftime("%b %d, %Y - %I:%M %p")
    elif lang_code.lower() == 'gb':
        # gb date format
        return date.strftime("%d %b %Y - %H:%M")
    elif lang_code.lower() == 'de':
        # de date format
        return date.strftime("%d.%m.%Y - %H:%M")
    else:
        # default case: uk date format
        return date.strftime("%d %b %Y - %H:%M")
