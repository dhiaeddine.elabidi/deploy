"""
File: category_model.py
Category model
"""
from app.models.database.tables.category import Category


class CategoryModel:
    """
    The model of category
    """

    @staticmethod
    def get_category(category_id=None):
        if category_id is None:
            raise TypeError("category_id cannot be None.")
        else:
            category = Category.query.filter_by(id=category_id).first()
            return {
                'code': category.code,
                'name': category.name,
                'description': category.description
            }

    @staticmethod
    def get_category_id(category_code=None):
        if category_code is None:
            raise TypeError("category_name cannot be None.")
        else:
            category = Category.query.filter_by(code=category_code).first()
            return category.id

    @staticmethod
    def get_categories_choices():
        results = list()
        categories = Category.query.all()
        for category in categories:
            results.append((category.id, category.name))
        return results

