"""
File: rsva_billing_model.py
rsva billing model (retrieve data from freeswitch)
"""
from app.models.database.tables.number import Number
from app.models.database.tables.rsva_number_invoice import RsvaNumberInvoice
from app.models.database.tables.rsva_user_invoice import RsvaUserInvoice
from app.models.database.tables.rsva_tariff import RsvaTariff
from app.utilities.commons import format_time
from app import db


class RSVAInvoiceModel:
    """
    The model of rsva billing
    """

    @staticmethod
    def get(user_id, month, year):
        numbers_detail = db.session.query(Number, RsvaNumberInvoice, RsvaTariff)\
            .join(RsvaTariff) \
            .join(RsvaNumberInvoice) \
            .filter(Number.user_id == user_id)\
            .filter(RsvaNumberInvoice.period_month == month)\
            .filter(RsvaNumberInvoice.period_year == year)\
            .all()
        user_invoice = RsvaUserInvoice.query\
            .filter_by(user_id=user_id, period_month=month, period_year=year)\
            .first()
        if user_invoice:
            invoice = {
                'description': {
                    'id': user_invoice.id,
                    'call_number': user_invoice.num_calls,
                    'call_duration': format_time(user_invoice.duration_calls),
                    'pre_value': round(user_invoice.pre_value, 2),
                    'discount': user_invoice.discount if user_invoice.discount else 0,
                    'vat': user_invoice.vat if user_invoice.vat else 0,
                    'value': round(user_invoice.value, 2),
                    'paid': 'Paid' if user_invoice.paid else 'Not paid',
                    'period': {
                        'month': user_invoice.period_month,
                        'year': user_invoice.period_year
                    }
                },
                'details': []
            }
            for number in numbers_detail:
                invoice['details'].append(
                    {
                        'country': number[0].country_fullname,
                        'country_code': number[0].country_code,
                        'number': number[0].number,
                        'tariff': number[2].rsva_tariff_end_user_code,
                        'call_number': number[1].num_calls,
                        'call_duration': format_time(number[1].duration_calls),
                        'value': round(number[1].value, 2)
                    }
                )
        else:
            invoice = None
        return invoice

    @staticmethod
    def get_all(user_id):
        user_invoices = RsvaUserInvoice.query \
            .filter_by(user_id=user_id) \
            .all()
        invoices = list()
        for invoice in user_invoices:
            invoices.append(RSVAInvoiceModel.get(user_id, invoice.period_month, invoice.period_year))
        return invoices
