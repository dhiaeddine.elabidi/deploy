"""
File: reset_password_form.py
Reset password form for the application
"""
from flask_wtf import FlaskForm
from wtforms import PasswordField, validators


class ResetPasswordForm(FlaskForm):
    """
    Reset password form model
    """
    old_password = PasswordField('Old Password', validators=[validators.Length(min=8, max=80),
                                                             validators.input_required()])
    new_password = PasswordField('New Password', validators=[validators.Length(min=8, max=80),
                                                             validators.input_required(),
                                                             validators.equal_to('confirm_password',
                                                                                 message="Passwords does not match")])
    confirm_password = PasswordField('Confirm Password', validators=[validators.Length(min=8, max=80)])
