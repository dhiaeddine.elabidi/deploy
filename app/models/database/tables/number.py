"""
File: number.py
Number database model
"""
from app.utilities.commons import get_unique_id
from app.models.database.tables.rsva_number_invoice import RsvaNumberInvoice
from app import db
from datetime import datetime


class Number(db.Model):
    """
    Class Number: Object for database mapping
    sql_alchemy model
    """
    id = db.Column(db.String(36), primary_key=True, unique=True, nullable=False, default=get_unique_id())
    country = db.Column(db.String(3), nullable=True, unique=False)
    country_fullname = db.Column(db.String(96), nullable=True, unique=False)
    country_code = db.Column(db.String(10), nullable=False, unique=False)
    number = db.Column(db.String(64), nullable=True, unique=False)
    last_modified = db.Column(db.DateTime, default=None)
    created_at = db.Column(db.DateTime, default=datetime.utcnow)
    # foreign keys
    user_id = db.Column(db.String(36), db.ForeignKey('user.id'), nullable=True, unique=False)
    rsva_tariff_id = db.Column(db.String(36), db.ForeignKey('rsva_tariff.id'), nullable=False, unique=False)
    # relationship
    number_rsva_number_invoice = db.relationship('RsvaNumberInvoice', backref='number_rsva_number_invoice')
