"""
File: log.py
Logger database model
"""

from app import db
from datetime import datetime


class Log(db.Model):
    """
    Class log: object for database mapping
    sql_alchemy model
    """
    id = db.Column(db.Integer, primary_key=True, unique=True)
    log_level = db.Column(db.String(10), nullable=False)
    message = db.Column(db.String(300), nullable=False)
    description = db.Column(db.String(1000), nullable=True)
    ip_address = db.Column(db.String(16), nullable=True)
    user_reference = db.Column(db.String(36), nullable=True)
    created_at = db.Column(db.DateTime, default=datetime.utcnow)
