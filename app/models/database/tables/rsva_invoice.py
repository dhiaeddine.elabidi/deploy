"""
File : rsva_Invoice.py
"""
from app import db
from datetime import datetime


class RsvaInvoice(db.Model):
    """
    Class Rsva: Object for database mapping
    sql_alchemy model
    """
    id = db.Column(db.Integer, db.Sequence('rsva_invoice_id', start=10000, increment=1), primary_key=True,
                   unique=True, nullable=False)
    rsva_user_invoice_id = db.Column(db.Integer, db.ForeignKey('rsva_user_invoice.id'), nullable=False, unique=True)
    rsva_number_invoice_id = db.Column(db.Integer, db.ForeignKey('rsva_number_invoice.id'), nullable=False, unique=True)
    created_at = db.Column(db.DateTime, default=datetime.utcnow)
