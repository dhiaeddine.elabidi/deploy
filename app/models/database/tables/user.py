"""
File: user.py
User database model
"""
from app.utilities.commons import get_unique_id, generate_password
from app.models.database.tables.user_detail import UserDetail
from app.models.database.tables.user_service import UserService
from app.models.database.tables.number import Number
from app.models.database.tables.rsva_user_invoice import RsvaUserInvoice
from app.models.database.tables.ticket import Ticket
from app import db, login_manager
from datetime import datetime
from flask_login import UserMixin


class User(UserMixin, db.Model):
    """
    Class User: object for database mapping
    sql_alchemy model
    Basic database table for user login
    """
    id = db.Column(db.String(36), primary_key=True, unique=True, default=get_unique_id())
    username = db.Column(db.String(64), unique=True, nullable=False)
    password = db.Column(db.String(80), default=generate_password(), nullable=False)
    lang = db.Column(db.String(3), nullable=False, unique=False, default='gb')
    valid = db.Column(db.Boolean, default=False, nullable=False)
    first_login = db.Column(db.Boolean, default=True)
    last_login = db.Column(db.DateTime, nullable=True, default=None)
    last_modified = db.Column(db.DateTime, nullable=True, default=None)
    created_at = db.Column(db.DateTime, default=datetime.utcnow)
    # foreign keys
    category_id = db.Column(db.Integer, db.ForeignKey('category.id'), nullable=False, unique=False)
    currency_id = db.Column(db.Integer, db.ForeignKey('currency.id'), nullable=False, unique=False)
    # relationships
    user_user_detail = db.relationship('UserDetail', backref='user_user_detail')
    user_user_service = db.relationship('UserService', backref='user_user_service')
    user_number = db.relationship('Number', backref='user_number')
    user_ticket = db.relationship('Ticket', backref='user_ticket')
    user_rsva_user_invoice = db.relationship('RsvaUserInvoice', backref='user_rsva_user_invoice')

    def get(self):
        """Return the email address to satisfy Flask-Login's requirements."""
        return self.id


@login_manager.user_loader
def load_user(user_id):
    """
    user load method
    :param user_id: user model id
    :return: instance
    """
    try:
        return User.query.get(user_id)
    except:
        return None
