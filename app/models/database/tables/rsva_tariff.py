"""
File: call_rate.py
Call rate database model
"""
from app.utilities.commons import get_unique_id
from app.models.database.tables.number import Number
from app import db
from datetime import datetime


class RsvaTariff(db.Model):
    """
    Call rate: Object for database mapping
    sql_alchemy model
    """
    id = db.Column(db.String(36), primary_key=True, unique=True, nullable=False, default=get_unique_id())
    last_modified = db.Column(db.DateTime, default=datetime.utcnow)
    created_at = db.Column(db.DateTime, default=datetime.utcnow)
    # foreign keys
    rsva_tariff_end_user_code = db.Column(db.String(6), db.ForeignKey('rsva_tariff_end_user.code'),
                                          nullable=False, unique=False)
    rsva_tariff_client_id = db.Column(db.Integer, db.ForeignKey('rsva_tariff_client.id'),
                                      nullable=False, unique=False)
    # relationship
    rsva_tariff_number = db.relationship('Number', backref='rsva_tariff_number')
