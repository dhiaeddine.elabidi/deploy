"""
File: service.py
Service database model
"""
from app import db
from datetime import datetime


class Service(db.Model):
    """
    Currency class: Object for database mapping
    sql_alchemy model
    """
    id = db.Column(db.Integer, primary_key=True)
    service_code = db.Column(db.String(4), nullable=False, unique=True)
    name = db.Column(db.String(60), nullable=False)
    level = db.Column(db.String(12), nullable=False, default="secondary")
    icon = db.Column(db.String(20), nullable=False, default="cogs")
    description = db.Column(db.String(250), default=None)
    created_at = db.Column(db.DateTime, default=datetime.utcnow)
